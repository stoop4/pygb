from tornado.web import gen
from common import checkloggedin, formatstr
from database import query_async
from json import dumps, loads
from uuid import uuid4

@gen.coroutine
def get(self):
    sc = checkloggedin(self)
    if not sc:
        return self.write('"ok" : false, "message" : "access denied"')

    sql = formatstr("SELECT * FROM bgrade AS g JOIN bguser AS u ON u.bguserid = g.bguserid WHERE u.username = ':username:'",
                    username=sc['username'])
    cursor = yield query_async(sql)

    grades = {}

    for i in cursor.fetchall():
        grade_uid = i['bgradeuid']
        value = i['value']
        received = i['received']
        weighting = i['weighting']
        subject_uid = i['bgsubjectuid']
        comment = i['comment']

        grades[grade_uid] = {
            'value' : value,
            'received' : str(received),
            'weighting' : weighting,
            'subjectuid' : subject_uid,
            'comment' : comment
        }

    self.write(dumps(grades))

@gen.coroutine
def post(self):
    sc = checkloggedin(self)
    if not sc:
        self.write('"ok" : false, "message" : "access denied"')

    grades = loads(self.request.body.decode())

    insert_grades = {}
    update_grades = {}
    db_grades = {}
    delete_grade_uids = []


    sql = formatstr("SELECT * FROM bgrade AS g JOIN bguser AS u ON u.bguserid = g.bguserid WHERE u.username = ':username:'",
                    username=sc['username'])
    cursor = yield query_async(sql)

    for i in cursor.fetchall():
        grade_uid = i['bgradeuid']
        value = i['value']
        received = i['received']
        weighting = i['weighting']
        subject_uid = i['bgsubjectuid']
        comment = i['comment']

        db_grades[grade_uid] = {
            'value' : value,
            'received' : received,
            'weighting' : weighting,
            'subjectuid' : subject_uid,
            'comment' : comment
        }

    for i in grades:
        grade = grades[i]
        uid = i

        if len(uid) == 5:
            uid = str(uuid4())
            insert_grades[uid] = grade
        else:
            db_grade = db_grades.get(i, False)
            if not db_grade:
                return self.write('INVALID ARGUMENT')

            for i in db_grade:
                if grade[i] != db_grade[i]:
                    update_grades[uid] = grade
                    break

    for i in db_grades:
        if not i in grades:
            delete_grade_uids.append(i)

    for i in insert_grades:
        uid = i
        grade = insert_grades[uid]
        sql = formatstr("INSERT INTO bgrade VALUES (':uid:', ':value:', ':received:', NOW(), :weighting:, ':subjectuid:', (SELECT bguserid FROM bguser WHERE username = ':username:'), false, ':comment:', NOW())",
                        uid=uid, value=grade['value'], received=grade['received'], weighting=grade['weighting'], subjectuid=grade['subjectuid'],
                        username=sc['username'], comment=grade['comment'])
        yield query_async(sql)


    for i in update_grades:
        uid = i
        grade = update_grades[uid]

        sql = formatstr("UPDATE bgrade SET value=':value:', received=':received:', weighting=:weighting:, bgsubjectuid=':subjectuid:', comment=':comment:', edited=NOW() WHERE bgradeuid = ':uid:'",
                        value=grade['value'], received=grade['received'], weighting=grade['weighting'], subjectuid=grade['subjectuid'], comment=grade['comment'], uid=uid)

        yield query_async(sql)

    for uid in delete_grade_uids:
        sql = formatstr("DELETE FROM bgrade WHERE bgradeuid = ':uid:'", uid=uid)

        yield query_async(sql)

    self.write('"ok" : true')



