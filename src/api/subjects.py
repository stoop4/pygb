from tornado.web import gen
from common import checkloggedin, formatstr
from database import query_async
from json import dumps, loads

@gen.coroutine
def get(self):
    sc = checkloggedin(self)
    if not sc:
        return self.write('{"error" : true, "message" : "access denied"}')

    username = sc['username']
    sql = formatstr("SELECT s.subjectname, s.bgsubjectuid FROM bgsubject as s JOIN bguser_bgsubject as bs on bs.bgsubjectuid = s.bgsubjectuid JOIN bguser AS u ON u.bguserid = bs.bguserid WHERE u.username = ':username:'",
                    username = username)
    cursor = yield query_async(sql)

    visited = {}

    for i in cursor.fetchall():
        uid = i['bgsubjectuid']
        name = i['subjectname']
        visited[uid] = name


    sql = "SELECT subjectname, bgsubjectuid FROM bgsubject"
    cursor = yield query_async(sql)

    unvisited = {}

    for i in cursor.fetchall():
        uid = i['bgsubjectuid']
        name = i['subjectname']
        if uid not in visited:
            unvisited[uid] = name

    return self.write(dumps({'visited' : visited, 'unvisited' : unvisited}))

@gen.coroutine
def post(self):
    sc = checkloggedin(self)
    if not sc:
        return self.write('{"error" : true, "message" : "access denied", "ok" : false}')

    data = loads(self.request.body.decode())
    visited = data['visited']
    unvisited = data['unvisited']


    for uid in unvisited:
        sql = formatstr("DELETE FROM bguser_bgsubject WHERE bgsubjectuid = ':uid:'", uid=uid)
        yield query_async(sql)

        sql = formatstr("DELETE FROM bgrade WHERE bgsubjectuid = ':uid:' AND bguserid = (SELECT bguserid FROM bguser WHERE username = ':username:')",
                    uid=uid, username= sc['username'])
        yield query_async(sql)


    username = sc['username']
    sql = formatstr("SELECT s.subjectname, s.bgsubjectuid FROM bgsubject as s JOIN bguser_bgsubject as bs on bs.bgsubjectuid = s.bgsubjectuid JOIN bguser AS u ON u.bguserid = bs.bguserid WHERE u.username = ':username:'",
                    username = username)
    cursor = yield query_async(sql)

    db_visited = {}

    for i in cursor.fetchall():
        uid = i['bgsubjectuid']
        name = i['subjectname']
        db_visited[uid] = name

    for uid in visited:
        if uid not in db_visited:
            sql = formatstr("INSERT INTO bguser_bgsubject VALUES ((SELECT bguserid FROM bguser WHERE username = ':username:'),':uid:')",
                            uid=uid, username=username)
            yield query_async(sql)

    self.write('{"ok" : true}')