from common import dbconnection, formatstr, config, createhash
from json import loads
from tornado import gen
import tornado_mysql
from uuid import uuid4

cfg = config()


def create_tables():
    connection = dbconnection()
    with connection.cursor() as cursor:
        def create_table(table_name, attributes):
            str_attrib = ','.join(attributes)

            sql = formatstr('CREATE TABLE IF NOT EXISTS :tablename: (:tables:)',
                            tablename=table_name,
                            tables=str_attrib)

            cursor.execute(sql)

        with open('database.json') as file:
            database = loads(file.read())['tables']
            for table in database:
                create_table(table['name'], table['attributes'])

            cursor.close()
            connection.commit()
            connection.close()

def initialize_database():
    connection = dbconnection()
    iu = cfg['root_user']
    root_uid = str(uuid4())
    users_uid = str(uuid4())
    sql_root = formatstr("INSERT INTO user values(':uid:', ':username:', ':passwordhash:', true)",
                    username=iu['username'],
                    passwordhash=createhash(iu['password']),
                    uid=root_uid)
    sql_users = formatstr("INSERT INTO sgroup values(':uid:', 'users', true)",
                          uid=users_uid)


    with connection.cursor() as cursor:
        cursor.execute(sql_root)
        cursor.execute(sql_users)
        cursor.close()
        connection.commit()
        connection.close()



@gen.coroutine
def query_async(sql):
    dbcfg = cfg['database']
    connection = yield tornado_mysql.connect(host=dbcfg['host'],
                                             port=dbcfg['port'],
                                             user=dbcfg['user'],
                                             passwd=dbcfg['password'],
                                             db=dbcfg['db'])
    cursor = connection.cursor(tornado_mysql.cursors.DictCursor)
    yield cursor.execute(sql)
    cursor.close()
    yield connection.commit()
    connection.close()
    return cursor