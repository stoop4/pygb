from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop
from webhandler import gethandlers
from common import config
from os.path import isfile
from database import create_tables, create_initial_user

cfg = config()

if __name__ == "__main__":
    fn = cfg['isconfigured_filename']
    if not isfile(fn):
        create_tables()
        create_initial_user()
        open(fn, "w").close()


    app = Application(gethandlers(), cookie_secret=cfg['cookiesecret'])
    app.listen(cfg['port'])
    IOLoop.current().start()

