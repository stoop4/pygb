from hashlib import sha256
from json import load
from pymysql import connect, cursors
from os.path import join
from tornado.web import gen
from database import query_async


SALT = ''
LANG = load(open(join('lang', 'language.json')))
STRINGS = LANG['strings']

def getstr(name, default=False):
    return STRINGS.get(name, default)

def getnavs(active='start'):
    d  = {
        'start' : {'text' : getstr('start'), 'href' : '/'},
        'subjects': {'text' : getstr('manage-subjects'), 'href' : '/subjects'},
        'grades' : {'text' : getstr('grades'), 'href' : '/grades'}
    }
    for i in d:
        d[i]['active'] = False
    d[active]['active'] = True
    return [d[i] for i in d]

def formatstr(format, **kwargs):
    for n in kwargs:
        format = format.replace(':'+n+':', kwargs[n])

    return format

def createhash(plain):
    return sha256((SALT+plain).encode()).hexdigest()

def config():
    return load(open('config.json'))

cfg = config()

def dbconnection():
    dbconfig = cfg['database']
    return connect(host=dbconfig['host'],
                     user=dbconfig['user'],
                     password=dbconfig['password'],
                     db=dbconfig['db'],
                     cursorclass=cursors.DictCursor)

sessions = {}

def savesession(sessionid, username, **kwargs):
    sessions[sessionid] = {'username' : username}
    for i in kwargs:
        sessions[sessionid][i] = kwargs[i]

def checksession(sessionid):
    usid = sessionid.decode()
    sessarg = sessions.get(usid, False)
    if sessarg:
        sessarg['sessionid'] = usid
    return sessarg

def checkloggedin(handler):
    sc = handler.get_secure_cookie('session')
    sessargs = None
    if sc:
        sessargs = checksession(sc)

    return sessargs

def destroysession(sessioid):
    del sessions[sessioid]

@gen.coroutine
def checkright(rightuid, useruid):
    sql = formatstr("SELECT * FROM rightassignment WHERE srightuid = ':rightuid:''",
                    rightuid=rightuid)
    cursor = yield query_async(sql)
    right = cursor.fetchone()
    if not right:
        raise Exception("Invalid Right UID")

    if right['useruid'] == useruid:
        return True

    groupuid = right['sgroupuid']

    sql = formatstr("SELECT count(u.useruid) AS ingroup FROM user AS u JOIN user_sgroup as s ON s.useruid = u.useruid WHERE s.sgroupuid = ':sgroupuid:' AND u.useruid = ':useruid:'",
                    sgroupuid=groupuid)
    cursor = yield query_async(sql)
    ingroup = cursor.fetchone()['ingroup'] == '1'
    if ingroup:
        return True

    return False

cfg = config()
SALT = cfg['salt']
