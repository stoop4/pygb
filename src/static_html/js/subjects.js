var visited;
var unvisited;

function formatstr(format, args) {
  Object.keys(args).forEach(function(el){
    format = format.split(':'+el+':').join(args[el]);
  })
  return format
}
function updateview() {
  v = $('#visited');
  u = $('#unvisited');
  t = $('#litemplate').html();

  v.html('');
  u.html('');

  Object.keys(visited).forEach(function(el){
    v.append(formatstr(t, {
      subjname : visited[el],
      uid : el,
      action : STRREMOVE,
      btntype : 'btn-danger'
    }))
  })

  Object.keys(unvisited).forEach(function(el){
    u.append(formatstr(t, {
      subjname : unvisited[el],
      uid : el,
      action : STRADD,
      btntype : 'btn-default'
    }))
  })

}
function setvisited(uid) {
    var el = unvisited[uid];
    delete unvisited[uid];
    visited[uid] = el;
    updateview();
  }
function setunvisited(uid) {
    var el = visited[uid];
    delete visited[uid];
    unvisited[uid] = el;
    updateview();
  }
function toggle_visited(uid) {
    if (visited[uid]){
         $('#delete-subject').attr('onclick', formatstr("setunvisited(':uid:');$('#delete-subject-modal').modal('hide');", {uid:uid}));
         $('#delete-subject-modal').modal();
    }
    else if (unvisited[uid])
      setvisited(uid);
    else
      console.log('Invalid UID'+uid);
  }
function update_data() {
    $.get('/api/subjects', function(data){
        d = JSON.parse(data);
        visited = d.visited;
        unvisited = d.unvisited;
        updateview();
    })
}
function save() {
    $.post('/api/subjects', JSON.stringify({'visited' : visited, 'unvisited' : unvisited}),function(data){
        return console.log(data);
        if (!data.ok) {
            window.alert('ERROR: Please reload page')
        }
    })
}

$(document).ready(function(){
    update_data();
})

