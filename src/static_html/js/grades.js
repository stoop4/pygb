var grades;

load_data();

var open_grade = false;
var unsaved = false;

function formatstr(format, args) {
  Object.keys(args).forEach(function(el){
    format = format.split(':'+el+':').join(args[el]);
  })
  return format
}

function uid() {
    return Math.random().toString().substring(2, 7);
}

function start_edit(uid) {
    grade = grades[uid];

    open_grade = uid;

    $('#grade-value').val(grade.value);
    $('#grade-received').val(grade.received);
    $('#grade-weighting').val(grade.weighting);
    $('#grade-subject').val(grade.subjectuid);
    $('#grade-comment').val(grade.comment);

    $('#edit-grade-modal').modal();

    unsaved = true;
}

function end_edit() {
    uid = open_grade;
    grades[uid] = {
        value : $('#grade-value').val(),
        received : $('#grade-received').val(),
        weighting : $('#grade-weighting').val(),
        subjectuid : $('#grade-subject').val(),
        comment : $('#grade-comment').val()
    }
    open_grade = false;
    save();
    updateview();
}

function updateview() {
    t = $('#template-list-button').html();
    g = $('#grades');

    g.html('');
    Object.keys(grades).forEach(function(uid){
    grade = grades[uid];
        g.append(formatstr(t, {
            value : grade.value,
            date : grade.received,
            uid : uid
        }));

    })
}

function load_data() {
    $.get('/api/grades', function(str_data){
        grades = JSON.parse(str_data);
        updateview();
    })
}

function save() {
    $.post('/api/grades', JSON.stringify(grades), function(str_data){
        var data = JSON.parse(str_data);
        if (!data.ok)
            return window.alert('ERROR. Data not saved. Please reload page');

        unsaved = false;
    })
}

function new_grade() {
    var id = uid();
    grades[id] = {
        value : '',
        received : '',
        weighting : 100,
        comment : '',
        subjectuid : ''
    }

    start_edit(id);
}

function delete_grade(uid) {
    delete grades[uid];
    updateview();
    save();
}