from handler.adminhandler import AdminHandler, EditUserHandler, DeleteUserHandler, EditSchoolHandler
from handler.apihandler import ApiHandler
from handler.indexhandler import IndexHandler
from handler.subjectshandler import SubjectsHandler
from handler.gradeshandler import GradesHandler
from tornado.web import StaticFileHandler

from handler.loginhandler import LoginHandler, LogoutHandler


def gethandlers():
    return [
        (r"/static/(.*)", StaticFileHandler, {'path' : 'static_html'}),
        (r"/", IndexHandler),
        (r"/login", LoginHandler),
        (r'/admin', AdminHandler),
        (r'/admin/edituser/([^/]+)', EditUserHandler),
        (r'/logout', LogoutHandler),
        (r'/admin/deleteuser/([^/]+)', DeleteUserHandler),
        (r'/admin/editschool/([^/]+)', EditSchoolHandler),
        (r'/api/([^/]+)', ApiHandler),
        (r'/subjects', SubjectsHandler),
        (r'/grades', GradesHandler)
    ]