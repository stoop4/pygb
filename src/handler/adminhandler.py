from tornado.web import RequestHandler, gen
from tornado.template import Loader
from common import checkloggedin, getnavs, formatstr, createhash, getstr
from htmlmin import minify
from json import dumps, loads
from database import query_async
from uuid import uuid4

tploader = Loader('templates')

class AdminHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        sessargs = checkloggedin(self)
        if sessargs:
            if sessargs['securitylevel']>0:
                user={'username' : sessargs['username'], 'isadmin':True}
                cursor = yield query_async('SELECT bguserid, username FROM bguser')
                users = cursor.fetchall()
                cursor = yield query_async('SELECT * from bgschool')
                schools = cursor.fetchall()

                gen_tpl = tploader.load('admin.html').generate(title="Administration",
                                                               navs=getnavs(),
                                                               user=user,
                                                               users=users,
                                                               schools=schools)
                self.write(minify(gen_tpl.decode()))
            else:
                self.write('Computer says no: Access Denied. <a href="/">go home</a>')
        else:
            self.redirect(formatstr('/login?redir=:redir:', redir='/admin'))

class EditUserHandler(RequestHandler):
    @gen.coroutine
    def get(self, slug):
        sessargs = checkloggedin(self)
        if not sessargs:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sessargs['securitylevel']<= 0:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        securitylevels = ['user', 'School administrator', 'admin', 'root']

        if slug == 'new':
            user={'username' : sessargs['username'], 'isadmin':True}
            gen_temp = tploader.load('edituser.html').generate(edituser={'username' : '', 'bguserid' : 'new', 'securitylevel' : 0, 'isnew' : True},
                                                               user=user, navs=getnavs(),
                                                               title='Create user',
                                                               securitylevels=securitylevels)
            self.write(minify(gen_temp.decode()))
        else:

            cursor = yield query_async(formatstr('SELECT bguserid, username, securitylevel, issystem FROM bguser where bguserid = :userid:',
                                     userid=slug))
            current_user = cursor.fetchone()

            if not current_user:
                return self.write('Error. Invalid user. <a href="/admin">Go back</a>')
            else:
                user={'username' : sessargs['username'], 'isadmin':True}
                current_user['isnew'] = False

                gen_temp = tploader.load('edituser.html').generate(edituser=current_user,
                                                                   user=user, navs=getnavs(),
                                                                   title='Edit user'+current_user['username'],
                                                                   securitylevels=securitylevels)
                self.write(minify(gen_temp.decode()))
    @gen.coroutine
    def post(self, slug):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sc['securitylevel'] <2:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        if slug == 'new':
            username = self.get_argument('username', default=False)
            password = self.get_argument('password', default=False)
            securitylevel = self.get_argument('securitylevel', default=False)

            if not (username and securitylevel and password):
                return self.write('Error. Invalid data. <a href="/admin">go back</a>')


            sql = formatstr("SELECT bguserid FROM bguser WHERE username = ':username:'",
                            username=username)
            cursor = yield query_async(sql)

            if cursor.fetchone():
                self.write('Error: user exists. <a href="/admin/edituser/new">Try again</a>')
            else:
                sql = formatstr("INSERT INTO bguser VALUES (NULL, ':username:', ':passwordhash:', :securitylevel:, false)",
                                username=username,
                                passwordhash = createhash(password),
                                securitylevel = securitylevel)
                yield query_async(sql)
                self.redirect('/admin')

        else:
            username = self.get_argument('username', default=False)
            password = self.get_argument('password', default=False)
            securitylevel = self.get_argument('securitylevel', default=False)

            if not (username and securitylevel):
                return self.write('Error. Invalid data. <a href="/admin">go back</a>')

            sql = formatstr("UPDATE bguser set username=':username:', securitylevel=':securitylevel:' WHERE userid = :userid:",
                            username = username,
                            securitylevel=securitylevel,
                            userid=slug)
            yield  query_async(sql)

            if password:
                sql = formatstr("UPDATE bguser set passwordhash = ':passwordhash:' WHERE bguserid = :userid:",
                                passwordhash=createhash(password),
                                userid=slug)
                yield query_async(sql)
            self.redirect('/admin')


class DeleteUserHandler(RequestHandler):
    @gen.coroutine
    def get(self, slug):
        sc = checkloggedin(self)
        if not sc:
           return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sc['securitylevel'] <2:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        sql = formatstr('SELECT issystem FROM bguser WHERE bguserid = :userid:',
                        userid=slug)
        cursor = yield query_async(sql)
        result = cursor.fetchone()
        if not result:
            return self.write('Invalid Arguments')
        elif result['issystem']:
            return self.write('Do not delete system users')

        sql = formatstr("DELETE FROM bguser WHERE bguserid = :userid:",
                        userid=slug)
        yield query_async(sql)
        self.redirect('/admin')

class EditSchoolHandler(RequestHandler):
    @gen.coroutine
    def get(self, slug):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sc['securitylevel'] <2:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        sql = "SELECT * FROM bguser";
        cursor = yield query_async(sql)
        users = cursor.fetchall()

        if slug == 'new':
            user={'username' : sc['username'], 'isadmin':True}
            gen_tpl = tploader.load('editschool.html').generate(user=user, navs=getnavs(),
                                                                school={'bgschooluid' : 'new', 'schoolname' : 'new school', 'subjects' : [], 'schooladmin' : ''},
                                                                title='new school', users=users, getstr=getstr)
            return self.write(minify(gen_tpl.decode()))

        else:
            cursor = yield query_async(formatstr("SELECT * FROM bgschool WHERE bgschooluid = ':uid:'",
                                       uid=slug))
            school = cursor.fetchone()

            if not school:
                return self.write('Invalid Arguments')

            user={'username' : sc['username'], 'isadmin':True}

            cursor = yield query_async(formatstr("SELECT * FROM bgsubject WHERE bgschooluid = ':uid:'",
                                                 uid=slug))
            school['subjects'] = cursor.fetchall()

            gen_tpl = tploader.load('editschool.html').generate(user=user, navs=getnavs(),
                                                                school=school,
                                                                title='Edit '+school['schoolname'], users=users, getstr=getstr)

            return self.write(minify(gen_tpl.decode()))


    @gen.coroutine
    def post(self, slug):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sc['securitylevel'] <2:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        schoolname = self.get_argument('schoolname')
        schooladmin = self.get_argument('schooladmin')

        if slug == 'new':
            schooluid = str(uuid4())

            sql = formatstr("INSERT INTO bgschool values (':uid:', ':name:',':admin:')",
                            uid=schooluid, name=schoolname, admin=schooladmin)

            yield query_async(sql)

            return self.redirect('/admin')

        else:
            schooluid = slug

            sql = formatstr("UPDATE bgschool SET schoolname=':schoolname:', schooladmin=:schooladmin: WHERE bgschooluid = ':uid:'",
                            schooladmin=schooladmin, schoolname=schoolname, uid=schooluid)

            yield query_async(sql)

            return self.redirect('/admin')


class DeleteSchoolHandler(RequestHandler):
    def get(self, slug):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        if sc['securitylevel'] <2:
            return self.write('Computer says no: Access Denied. <a href="/">go home</a>')

        sql = formatstr("DELETE FROM bgschool WHERE bgschooluid = ':schooluid:'",
                        schooluid=slug)

        yield query_async(sql)

        sql = formatstr("DELETE FROM bgsubject WHERE bgschooluid = ':schooluid:",
                        schooluid=slug)


class EditSubjectHandler(RequestHandler):
    @gen.coroutine
    def get(self, slug):

        sc = checkloggedin(self)

        if not sc:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))


        sql = formatstr("SELECT * FROM bgsubject WHERE bgsubjectuid = ':uid:'",
                        uid=slug)
        cursor = yield query_async(sql)

        subject = cursor.fetchone()







