from tornado.web import gen, RequestHandler
from api.subjects import get as get_subjects, post as post_subjects
from api.grades import get as get_grades, post as post_grades

class ApiHandler(RequestHandler):
    @gen.coroutine
    def get(self, slug):
        if slug == 'subjects':
            yield get_subjects(self)
        elif slug == 'grades':
            yield get_grades(self)

    @gen.coroutine
    def post(self, slug):
        if slug == 'subjects':
            yield post_subjects(self)
        elif slug == 'grades':
            yield post_grades(self)




