from tornado.web import RequestHandler
from tornado.template import Loader
from htmlmin import minify
from common import getstr, getnavs, checkloggedin, formatstr


LOADER = Loader('templates')

class SubjectsHandler(RequestHandler):
    def get(self):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:loc:',
                                           loc=self.request.uri))

        user = {'username' : sc['username'], 'isadmin' : sc['securitylevel']>0}
        gen_tpl = LOADER.load('subjects.html').generate(title=getstr('subjects'),
                                                        navs=getnavs('subjects'),
                                                        getstr=getstr,
                                                        user=user)
        self.write(minify(gen_tpl.decode()))

