from tornado.web import RequestHandler
from tornado.template import Loader
from common import getnavs, formatstr, createhash, savesession, checkloggedin, destroysession
from database import query_async
from uuid import uuid4
from htmlmin import minify
from tornado.gen import coroutine

tploader = Loader('templates')

class LoginHandler(RequestHandler):
    def get(self):
        redir = self.get_query_argument('redir', default='/')
        err = self.get_query_argument('err', default=False)
        if checkloggedin(self):
            return self.redirect(redir)

        gen_temp = tploader.load('login.html').generate(title='log in', navs=getnavs(),
                                                        user=False, redir=redir, error=err)
        self.write(minify(gen_temp.decode()))

    @coroutine
    def post(self):
        redir = self.get_query_argument('redir', '/')

        username = self.get_argument('username')
        password = self.get_argument('password')
        hash = createhash(password)
        login_successed = False
        securitylevel = False

        sql = formatstr("SELECT passwordhash, useruid from user where username = ':username:'", username=username)
        cursor = yield  query_async(sql)

        result = cursor.fetchone()
        if result:
            db_passwordhash = result['passwordhash']
            db_useruid = result['useruid']

            if db_passwordhash == hash:
                login_successed = True


        if login_successed:
            sid = str(uuid4())
            self.set_secure_cookie('session', sid)
            savesession(sid, username, useruid=db_useruid)
            return self.redirect(redir)
        else:
            self.write('Error. Invalid credentials. Try again: <a href="/login">link</a>')






class LogoutHandler(RequestHandler):
    def get(self):
        sessargs = checkloggedin(self)
        if sessargs:
            destroysession(sessargs['sessionid'])
            self.clear_cookie('session')
            return self.redirect('/login')
        return self.redirect('/login')
