from tornado.web import RequestHandler, gen
from tornado.template import Loader
from common import checkloggedin, formatstr, getnavs, getstr
from htmlmin import minify
from database import query_async

LOADER = Loader('templates')


class GradesHandler(RequestHandler):
    @gen.coroutine
    def get(self):
        sc = checkloggedin(self)
        if not sc:
            return self.redirect(formatstr('/login?redir=:uri:', uri=self.request.uri))

        user = {
            'username' : sc['username'],
            'isadmin' : sc.get('securitylevel') > 0
        }

        sql = formatstr("SELECT * FROM bgsubject AS s JOIN bguser_bgsubject AS bb ON s.bgsubjectuid = bb.bgsubjectuid JOIN bguser AS u ON bb.bguserid = u.bguserid WHERE u.username = ':username:'",
                        username=sc['username'])
        cursor = yield query_async(sql)

        gen_templ = LOADER.load('grades.html').generate(user=user, navs=getnavs('grades'),
                                                        getstr=getstr, title=getstr('grades'), subjects=cursor.fetchall())

        self.write(gen_templ)
