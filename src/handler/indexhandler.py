from tornado.web import RequestHandler
from common import getnavs, checkloggedin, formatstr
from tornado.template import Loader
from htmlmin import minify

tploader = Loader('templates')

class IndexHandler(RequestHandler):
    def get(self):
        sessargs = checkloggedin(self)

        if not sessargs:
            return self.redirect(formatstr('/login?redir=:redir:', redir=self.request.uri))

        user = {
            'username' : sessargs['username'],
            'isadmin' : sessargs.get('securitylevel') > 0
        }

        gen_templates = tploader.load('start.html').generate(title="Title", navs=getnavs(), user=user)
        self.write(minify(gen_templates.decode()))

        # self.render(join('templates', 'start.html'), title="Title", navs=getnavs(), user=user)
